0.3
------
- Added optional connection parameter for migration command
- Updated readme

0.2
------
- Added `refresh` method on `Activation` facade
- Updated readme