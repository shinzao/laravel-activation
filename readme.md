# Laravel account activation (double opt-in)

This package adds account activation/validation (double opt-in) for the laravel auth scaffold.
It uses notifications to send the activation link to the user.

## Requirements
- PHP >= 5.6
- Laravel >= 5.3
- Working laravel default auth scaffold (see https://laravel.com/docs/5.3/authentication for setup instructions)

## Installation

Add the package to your  `composer.json`:
```
composer require enso/activation
```

## Setup

Add this line to the providers array in `config/app.php`:
```
Enso\Activation\ActivationServiceProvider::class,
```
Add this line to the aliases array in `config/app.php`:
```
'Activation' => Enso\Activation\Facades\Activation::class,
```
Publish the package configuration and the default notification and controller classes:
```
php artisan vendor:publish
```
Run the package migrate command to add a activation token column to your users table.
If you already have a column for this information, or you dont want to use the default 
name (activation_token), you can set it in `config/activation.php` before running the command.  
You can also use the optional `--database=DATABASE` parameter, where DATABASE is your connection. 
By default, the command uses the connection which is set in your environment.
```
php artisan activation:migrate
```
Add the activation route to your `routes/web.php` file:
```
Activation::routes();
```
In your `Http/Controllers/Auth/LoginController.php` replace
- the `Illuminate\Foundation\Auth\AuthenticatesUsers` trait with
- the `Enso\Activation\Auth\AuthenticatesUsers` trait

to avoid that users can login before they activated/validated their account.

In your `Http/Controllers/Auth/RegisterController.php` replace
- the `Illuminate\Foundation\Auth\RegistersUsers` trait with
- the `Enso\Activation\Auth\RegistersUsers` trait

to make sure, that an activation token is set and the user gets an activation notification on register.

## Available methods

You can use the following methods on the `Activation` facade:

- `Activation::activate(User $user, $activation_token)`  
Activates the user.
This method is called in the default `Http\Auth\ActivationController`, so usually you dont have to use it manually. 
However you can use it anywhere else where you need to activate a user.
- `Activation::refresh()`  
Generates a new activation token for the currently logged in user, sends a new activation notification and logs out the user.
The user has to activate his account again.  
You should use this method whenever a user needs to re-validate (in most cases due to a changed e-mail address). 

## Customization

If necessary, you can customize the following files to your needs:
- `Http/Controllers/Auth/ActivationController.php`  
The default controller that handles the activation link.
- `Notifications/Activation.php`  
The notification that sends the activation link to your users. It uses the default laravel functionality, so you can customize it like any other notification. By default it uses the mail channel.