<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use Activation;

class ActivationController extends Controller
{
    /**
     * Try to activate the user with the specified token
     *
     * @param User $user
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(User $user, $token)
    {
        if (Activation::activate($user, $token)) {
            return redirect()->route('login')->with('status', 'Your account was successfully activated.');
        }

        return redirect('/')->with('status', 'Unknown user or wrong activation token.');
    }
}
